<?php

namespace MutationTesting;

class Infectuous
{
    public static function doSomething(array $param): int
    {
        if (isset($param['test'])) {
            return 2;
        }

        return 1;
    }

    public static function doSomethingElse(array $param): array
    {
        if (isset($param['test'])) {
            return ['test' => $param['test']];
        }
        return [];
    }
}
