<?php

declare(strict_types=1);

use MutationTesting\Infectuous;
use PHPUnit\Framework\TestCase;

//
//it('should test something', function (): void {
//    $actual = Infectuous::doSomething([]);
//    expect($actual)->not()->toBeEmpty();
//});
//
//it('should test something something', function (): void {
//    $actual = Infectuous::doSomething(['test' => 1]);
//    expect($actual)->not()->toBeEmpty();
//});
//
//
//it('should test something else', function (): void {
//    $actual = Infectuous::doSomethingElse(['test' => 'a']);
//    expect($actual)->toHaveKey('test');
//});

class InfectuousTest extends TestCase {
    public function testItShouldTestSomething(): void {
        $actual = Infectuous::doSomething([]);
        $this->assertNotEmpty($actual);
    }

    public function testItShouldTestSomethingMore(): void {
        $actual = Infectuous::doSomething(['test' => 1]);
        $this->assertNotEmpty($actual);
    }

    public function testItShouldTestSomethingElse(): void {
        $actual = Infectuous::doSomethingElse([]);
        $this->assertEmpty($actual);
    }

    public function testItShouldTestSomethingElseMore(): void {
        $actual = Infectuous::doSomethingElse(['test' => 1]);
        $this->assertNotEmpty($actual);
    }
}